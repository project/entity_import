<?php

declare(strict_types=1);

namespace Drupal\entity_import\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\entity_import\Entity\EntityImporterInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Define the entity import controller.
 */
class EntityImportController extends ControllerBase {

  /**
   * Render a list of importers.
   */
  public function importerPages(): array {
    $build = [
      '#theme' => 'entity_import_list',
    ];
    $content = [];

    foreach ($this->getDisplayPageEntityImporters() as $identifier => $importer) {
      $content[$identifier] = $importer;
    }
    $build['#content'] = $content;

    return $build;
  }

  /**
   * Get display page entity importers.
   *
   * @return \Drupal\entity_import\Entity\EntityImporterInterface[]
   *   An array of entity importer which wants to expose to the frontend.
   */
  protected function getDisplayPageEntityImporters(): array {
    return $this
      ->entityTypeManager()
      ->getStorage('entity_importer')
      ->loadByProperties(['expose_importer' => TRUE]);
  }

  /**
   * Generate and download the import template.
   *
   * @param \Drupal\entity_import\Entity\EntityImporterInterface $entity_importer
   *   Entity importer object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response object.
   */
  public function downloadTemplate(EntityImporterInterface $entity_importer): Response {
    $headline = [];

    foreach ($entity_importer->getFieldMappingUniqueIdentifiers() as $identifier) {
      $headline[] = $identifier['identifier_name'];
    }

    foreach ($entity_importer->getFieldMapping() as $field) {
      $source = $field->get('source');
      if (!in_array($source, $headline, TRUE)) {
        $headline[] = $source;
      }
    }

    $filename = 'import_template_' . $entity_importer->id() . '.csv';
    $response = new Response();
    $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
    $response->headers->set('Content-Disposition', $disposition);
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Expires', '0');
    $response->headers->set('Content-Transfer-Encoding', 'binary');
    $response->setContent(implode(',', $headline));

    return $response;
  }

}
