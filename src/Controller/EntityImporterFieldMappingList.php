<?php

namespace Drupal\entity_import\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\entity_import\Form\EntityImporterOptionsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Define the entity import mapping list controller.
 */
class EntityImporterFieldMappingList extends ConfigEntityListBuilder {

  /**
   * The request stack.
   */
  protected RequestStack $requestStack;

  /**
   * The form builder service.
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type,): static {
    $instance = parent::createInstance($container, $entity_type);
    $instance->requestStack = $container->get('request_stack');
    $instance->formBuilder = $container->get('form_builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      $this->t('Label'),
      $this->t('Machine Name'),
      $this->t('Source Name'),
      $this->t('Destination'),
      $this->t('Target Bundle'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\entity_import\Entity\EntityImporterFieldMappingInterface $entity */
    return [
      $entity->label(),
      $entity->name(),
      $entity->getSource(),
      $entity->getDestination(),
      $entity->getImporterBundle(),
    ] + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    return parent::render() + [
      'options' => $this->buildFieldMappingOptionForm(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds(): array {
    $request = $this->requestStack->getCurrentRequest();
    if ($request?->attributes->has('entity_importer')) {
      $entity_importer_type = $request?->get('entity_importer');

      $query = $this->getStorage()->getQuery()
        ->condition('importer_type', $entity_importer_type)
        ->sort($this->entityType->getKey('id'));

      if ($this->limit) {
        $query->pager($this->limit);
      }
      $query->accessCheck(TRUE);
      return $query->execute();
    }

    return [];
  }

  /**
   * Builder field mapping option form.
   *
   * @return array
   *   The form render array.
   */
  protected function buildFieldMappingOptionForm(): array {
    $request = $this->requestStack->getCurrentRequest();
    $entity_importer_type = $request?->get('entity_importer');

    return $this->formBuilder->getForm(
      EntityImporterOptionsForm::class,
      $entity_importer_type
    );
  }

}
