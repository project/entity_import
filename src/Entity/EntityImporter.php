<?php

namespace Drupal\entity_import\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\entity_import\EntityImportSourceManagerInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;
use function is_array;

/**
 * Define entity configuration importer.
 *
 * @ConfigEntityType(
 *   id = "entity_importer",
 *   label = @Translation("Entity Importer"),
 *   config_prefix = "type",
 *   admin_permission = "administer entity import",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "expose_importer",
 *     "migration_source",
 *     "migration_entity",
 *     "migration_dependencies"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\entity_import\Form\EntityImporterForm",
 *       "edit" = "\Drupal\entity_import\Form\EntityImporterForm",
 *       "delete" = "\Drupal\entity_import\Form\EntityImporterDeleteForm",
 *     },
 *     "list_builder" = "\Drupal\entity_import\Controller\EntityImporterList",
 *     "route_provider" = {
 *       "html" = "\Drupal\entity_import\Entity\Routing\EntityImporterRouteDefault"
 *     }
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/entity-importer",
 *     "add-form" = "/admin/config/system/entity-importer/add",
 *     "edit-form" = "/admin/config/system/entity-importer/{entity_importer}/edit",
 *     "delete-form" = "/admin/config/system/entity-importer/{entity_importer}/delete"
 *   }
 * )
 */
class EntityImporter extends EntityImporterConfigEntityBase implements EntityImporterInterface {

  /**
   * The entity importer ID.
   */
  public string $id;

  /**
   * The entity importer label.
   */
  public string $label;

  /**
   * The entity importer description.
   */
  public string $description = '';

  /**
   * The entity importer display page boolean.
   */
  public bool $expose_importer = FALSE;

  /**
   * An array of source information.
   */
  public array $migration_source = [];

  /**
   * An array of entity information.
   */
  public array $migration_entity = [];

  /**
   * The importer migration dependencies.
   */
  public array $migration_dependencies = [];

  /**
   * Has importer page display changed.
   */
  protected bool $pageDisplayChanged;

  /**
   * {@inheritdoc}
   */
  public function exposeImporter(): bool {
    // Support backwards compatibility as $this->display_page is no longer the
    // structure based on recent changes. After entity save, it'll be removed
    // as it's not defined in the config_export directive upon save.
    if (property_exists($this, 'display_page')) {
      return $this->display_page;
    }
    return $this->expose_importer;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationSource(): array {
    // Support backwards compatibility as $this->source is no longer the
    // structure based on recent changes. After entity save, it'll be removed
    // as it's not defined in the config_export directive upon save.
    if (property_exists($this, 'source')) {
      return $this->source;
    }

    return $this->migration_source['source'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationEntity(): array {
    // Support backwards compatibility as $this->entity is no longer the
    // structure based on recent changes. After entity save, it'll be removed
    // as it's not defined in the config_export directive upon save.
    if (property_exists($this, 'entity')) {
      return $this->entity;
    }

    return $this->migration_entity['entity'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationPluginId($bundle): string {
    return "entity_import:{$this->id()}:{$bundle}";
  }

  /**
   * {@inheritdoc}
   */
  public function getImporterSourcePluginId(): mixed {
    return $this->getSourceInfoValue('plugin_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getImporterSourceConfiguration(): array {
    return $this->getSourceInfoValue('configuration', []);
  }

  /**
   * {@inheritdoc}
   */
  public function getImporterBundles(): array {
    return $this->getEntityInfoValue('bundles', []);
  }

  /**
   * {@inheritdoc}
   */
  public function getImporterEntityType(): mixed {
    return $this->getEntityInfoValue('type');
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationDependencies(): array {
    return $this->migration_dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function hasPageDisplayChanged(): bool {
    return $this->pageDisplayChanged;
  }

  /**
   * {@inheritdoc}
   */
  public function createUrl($route_name): Url {
    return Url::fromRoute($route_name, [
      'entity_importer' => $this->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function createLink($text, $route_name): Link {
    return Link::createFromRoute($text, $route_name, [
      'entity_importer' => $this->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    $this->pageDisplayChanged = $this->exposeImporter() === TRUE;

    if (!$this->isNew()) {
      /** @var \Drupal\entity_import\Entity\EntityImporterInterface $original */
      $original = $storage->loadUnchanged($this->getOriginalId());
      $this->pageDisplayChanged = $this->exposeImporter() !== $original->exposeImporter();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    $this->onChange();
    parent::postSave($storage, $update);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    parent::delete();
    $this
      ->deleteOption()
      ->deleteFieldMapping();

    $this->onChange();
  }

  /**
   * {@inheritdoc}
   */
  public function onChange(): EntityImporterInterface {
    $this->clearMigrationPluginDiscovery();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createMigrationInstance($bundle, array $definition = []): MigrationInterface {
    $migration_definition = $this->getMigrationDefinition($bundle, $definition);

    return $this
      ->migrationPluginManager()
      ->createStubMigration($migration_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstBundle(): string {
    $bundles = $this->getImporterBundles();
    return reset($bundles);
  }

  /**
   * {@inheritdoc}
   */
  public function hasFieldMappings(): bool {
    return !empty($this->getFieldMapping());
  }

  /**
   * {@inheritdoc}
   */
  public function hasMultipleBundles(): bool {
    return count($this->getImporterBundles()) > 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMappingOptions(): array {
    $options = [];

    foreach ($this->getFieldMapping() as $field_mapping) {
      $options[$field_mapping->name] = $field_mapping->label();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapping(): array {
    return $this->getFieldMappingStorage()
      ->loadMultiple($this->getFieldMappingByImporterType());
  }

  /**
   * {@inheritdoc}
   */
  public function getMissingUniqueIdentifiers(array $identifiers): array {
    $missing = [];

    foreach ($this->getFieldMappingUniqueIdentifiers() as $values) {
      if (!isset($values['reference_type'])) {
        continue;
      }
      $identifier_name = $values['reference_type'] === 'field_type'
        ? $values['identifier_name']
        : $values['identifier_type'];

      if (!isset($identifiers[$identifier_name])) {
        $missing[] = $identifier_name;
      }
    }

    return $missing;
  }

  /**
   * {@inheritdoc}
   */
  public function hasFieldMappingUniqueIdentifiers(): bool {
    return !empty($this->getFieldMappingUniqueIdentifiers());
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMappingUniqueIdentifiers(): array {
    $unique_identifiers = $this
      ->getOptionsConfig()
      ->get('unique_identifiers.items');

    return $unique_identifiers ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencyMigrations(string $bundle, bool $order = TRUE, array $definition = []): array {
    $migrations = $this->createDependencyMigrations(
      $this->createMigrationInstance($bundle, $definition)
    );

    return !$order ? $migrations : array_reverse($migrations);
  }

  /**
   * {@inheritdoc}
   */
  public function loadDependencyMigration(string $plugin_id, string $bundle): ?MigrationInterface {
    return $this->getDependencyMigrations($bundle)[$plugin_id] ?? NULL;
  }

  /**
   * Create dependency migrations.
   */
  protected function createDependencyMigrations(
    MigrationInterface $migration,
    array $values = [],
    array $parents = [],
    array &$migrations = [],
  ) {
    $migrations[$migration->id()] = $migration;

    foreach ($migration->getMigrationDependencies()['optional'] as $plugin_id) {
      $configuration = NestedArray::getValue($values, $parents);

      /** @var \Drupal\migrate\Plugin\MigrationInterface $instance */
      $instance = $this
        ->migrationPluginManager()
        ->createInstance($plugin_id, $configuration);

      $this->createDependencyMigrations($instance, $values, $parents, $migrations);
    }

    return $migrations;
  }

  /**
   * Get importer option configuration.
   *
   * @param bool $editable
   *   Determine if the field mapping configurations are editable.
   */
  protected function getOptionConfig(bool $editable = FALSE): ImmutableConfig|Config {
    $name = $this->getOptionConfigName();
    $factory = static::getConfigManager()->getConfigFactory();

    return $editable ? $factory->getEditable($name) : $factory->get($name);
  }

  /**
   * Get the importer option config name.
   *
   * @return string
   *   The importer option config name.
   */
  protected function getOptionConfigName(): string {
    return 'entity_import.options.' . $this->id();
  }

  /**
   * Delete importer options.
   */
  protected function deleteOption(): EntityImporterInterface {
    $this->getOptionConfig(TRUE)->delete();

    return $this;
  }

  /**
   * Delete field mappings.
   */
  protected function deleteFieldMapping(): EntityImporterInterface {
    foreach ($this->getFieldMapping() as $field_mapping) {
      $field_mapping->delete();
    }

    return $this;
  }

  /**
   * Delete migration plugin discovery cache.
   */
  protected function clearMigrationPluginDiscovery(): EntityImporterInterface {
    $this
      ->getMigrationDiscoveryCache()
      ->delete('migration_plugins');

    return $this;
  }

  /**
   * Get field mapping by bundle.
   *
   * @param string $bundle
   *   The bundle name.
   */
  protected function getFieldMappingByBundle(string $bundle): array {
    return $this->getFieldMappingsKeyedByBundle()[$bundle] ?? [];
  }

  /**
   * Get field mapping keyed by bundle.
   */
  protected function getFieldMappingsKeyedByBundle(): array {
    $field_mappings = [];

    /** @var \Drupal\entity_import\Entity\EntityImporterFieldMappingInterface $field_mapping */
    foreach ($this->getFieldMapping() as $field_mapping) {
      $field_mappings[$field_mapping->getImporterBundle()][] = $field_mapping;
    }

    return $field_mappings;
  }

  /**
   * Get the migration definition.
   */
  protected function getMigrationDefinition(string $bundle, array $definition = []): array {
    if (!isset($bundle)) {
      throw new \RuntimeException(
        'The entity bundle is required for the migration definition.'
      );
    }
    $migration_definition = [
      'id' => $this->getMigrationPluginId($bundle),
      'label' => $this->t('@label: @bundle', [
        '@label' => $this->label(),
        '@bundle' => $bundle,
      ]),
      'source' => $this->getMigrateSourceDefinition(),
      'process' => $this->getMigrateProcessDefinition($bundle),
      'destination' => $this->getMigrateDestinationDefinition($bundle),
      'migration_dependencies' => $this->calculateMigrationDependencies($bundle),
    ];

    return array_merge_recursive($migration_definition, $definition);
  }

  /**
   * Get migrate source definition.
   *
   * @return array
   *   An array of full definition.
   */
  protected function getMigrateSourceDefinition(): array {
    $plugin_id = $this->getImporterSourcePluginId();

    return [
      'plugin' => $plugin_id,
      'importer_id' => $this->id(),
    ] + $this->getImporterSourceConfiguration();
  }

  /**
   * Get migrate destination definition.
   *
   * @param string $bundle
   *   The entity bundle name.
   *
   * @return array{plugin:string,default_bundle:string}
   *   An array of full definition.
   */
  protected function getMigrateDestinationDefinition(string $bundle): array {
    return [
      'plugin' => "entity:{$this->getImporterEntityType()}",
      'default_bundle' => $bundle,
    ];
  }

  /**
   * Get migrate process definition.
   *
   * @param string $bundle
   *   The entity bundle name.
   *
   * @return array
   *   An array of full definition.
   */
  protected function getMigrateProcessDefinition(string $bundle): array {
    $definition = [];

    foreach ($this->getFieldMappingByBundle($bundle) as $field_mapping) {
      if (!$field_mapping instanceof EntityImporterFieldMapping) {
        continue;
      }
      $processes = [];

      $source = trim($field_mapping->getSource());
      $configuration = $field_mapping->getProcessingConfiguration();

      // Iterate over all processes that were configured in a field mapping.
      if (isset($configuration['plugins'])
        && is_array($configuration['plugins'])) {
        $count = 1;

        foreach ($configuration['plugins'] as $plugin_id => $info) {
          $process = [
            'plugin' => $plugin_id,
          ] + array_filter($info['settings']);

          // Only add the source directive to the first process plugin. The
          // rest of the processes will inherit the value from the pipeline.
          if ($count === 1) {
            $process['source'] = $source;
          }
          $count++;

          $processes[] = $process;
        }
      }

      $definition[$field_mapping->getDestination()] = $processes !== []
        ? $processes
        : $source;
    }

    return $definition;
  }

  /**
   * Calculate migration dependencies.
   *
   * @param string $bundle
   *   The entity bundle name.
   *
   * @return array
   *   An array of migration dependencies.
   */
  protected function calculateMigrationDependencies(string $bundle): array {
    $dependencies = [
      'optional' => [],
      'required' => [],
    ];

    // Add migration dependencies that were assigned on the importer.
    foreach ($this->getMigrationDependencies() as $level => $dependency) {
      $migration = array_values($dependency['migration']);
      $dependencies[$level] = array_merge(
        $dependencies[$level], $migration
      );
    }

    return array_merge_recursive(
      $dependencies,
      $this->getMigrationLookupDependencies($bundle)
    );
  }

  /**
   * Get migration lookup dependencies.
   *
   * @param string $bundle
   *   The entity bundle name.
   *
   * @return array
   *   An array of dependencies.
   */
  protected function getMigrationLookupDependencies(string $bundle): array {
    $plugin_id = 'entity_import_migrate_lookup';
    $dependencies = [
      'optional' => [],
    ];

    /** @var \Drupal\entity_import\Entity\EntityImporterFieldMapping $field_mapping */
    foreach ($this->getFieldMappingByBundle($bundle) as $field_mapping) {
      if (!$field_mapping->hasProcessingPlugin($plugin_id)) {
        continue;
      }
      $configuration = $field_mapping->getProcessingConfiguration();

      if (!isset($configuration['plugins'])) {
        continue;
      }
      $plugins = $configuration['plugins'];

      if (!isset($plugins[$plugin_id])) {
        continue;
      }
      $plugin = $plugins[$plugin_id];

      if (empty($plugin['settings']['migration'])) {
        continue;
      }
      $migrations = is_array($plugin['settings']['migration'])
        ? array_values($plugin['settings']['migration'])
        : [$plugin['settings']['migration']];

      $dependencies['optional'] = array_merge($dependencies['optional'], $migrations);
    }

    return $dependencies;
  }

  /**
   * Get entity source value.
   *
   * @param string $key
   *   The key name.
   * @param null $default
   *   The default value if key is not found.
   */
  protected function getSourceInfoValue(string $key, $default = NULL): mixed {
    return $this->getMigrationSource()[$key] ?? $default;
  }

  /**
   * Get entity info value.
   *
   * @param string $key
   *   The key name.
   * @param null $default
   *   The default value if key is not found.
   */
  protected function getEntityInfoValue(string $key, $default = NULL): mixed {
    return $this->getMigrationEntity()[$key] ?? $default;
  }

  /**
   * Get field mapping configurations by importer type.
   */
  protected function getFieldMappingByImporterType(): array {
    $query = $this->getFieldMappingStorage()
      ->getQuery()
      ->condition('importer_type', $this->id());
    $query->accessCheck(TRUE);
    return $query->execute();
  }

  /**
   * Get field mapping storage.
   */
  protected function getFieldMappingStorage(): EntityStorageInterface {
    return $this->entityTypeManager()
      ->getStorage('entity_importer_field_mapping');
  }

  /**
   * Get migration discovery cache backend.
   */
  protected function getMigrationDiscoveryCache(): CacheBackendInterface {
    return \Drupal::service('cache.discovery_migration');
  }

  /**
   * Get importer options configuration.
   */
  protected function getOptionsConfig(): ImmutableConfig {
    return \Drupal::config("entity_import.options.{$this->id()}");
  }

  /**
   * Entity import source manager.
   */
  protected function entityImportSourceManager(): EntityImportSourceManagerInterface {
    return \Drupal::service('entity_import.source.manager');
  }

  /**
   * Migration plugin manager.
   */
  protected function migrationPluginManager(): MigrationPluginManager {
    return \Drupal::service('plugin.manager.migration');
  }

}
