<?php

namespace Drupal\entity_import\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Define the entity import config entity base class.
 */
abstract class EntityImporterConfigEntityBase extends ConfigEntityBase {

  use StringTranslationTrait;

  /**
   * Determine if configuration exist already.
   */
  public function entityExist(string $id, array $element): bool {
    $identifier = isset($element['#prefix_id'])
      ? "{$element['#prefix_id']}.{$id}"
      : $id;

    return $this->checkIfEntityIdExist($identifier);
  }

  /**
   * Check if entity identifier exist.
   */
  public function checkIfEntityIdExist(string $identifier): bool {
    return (bool) $this->getQuery()->condition('id', $identifier)->execute();
  }

  /**
   * Get entity storage query.
   */
  protected function getQuery(): QueryInterface {
    return $this->getStorage()->getQuery();
  }

  /**
   * Get entity storage.
   */
  protected function getStorage(): EntityStorageInterface {
    return $this->entityTypeManager()
      ->getStorage($this->getEntityTypeId());
  }

}
