<?php

namespace Drupal\entity_import\Entity;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Define entity importer field mapping.
 *
 * @ConfigEntityType(
 *   id = "entity_importer_field_mapping",
 *   label = @Translation("Entity importer field mapping"),
 *   config_prefix = "field_mapping",
 *   admin_permission = "administer entity import",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "name",
 *     "source",
 *     "processing",
 *     "destination",
 *     "importer_type",
 *     "importer_bundle"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\entity_import\Form\EntityImporterFieldMappingForm",
 *       "edit" = "\Drupal\entity_import\Form\EntityImporterFieldMappingForm",
 *       "delete" = "\Drupal\entity_import\Form\EntityImporterFieldMappingDeleteForm"
 *     },
 *     "list_builder" = "\Drupal\entity_import\Controller\EntityImporterFieldMappingList",
 *     "route_provider" = {
 *       "html" = "\Drupal\entity_import\Entity\Routing\EntityImporterRouteDefault"
 *     }
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/entity-importer/{entity_importer}/field-mapping",
 *     "add-form" =  "/admin/config/system/entity-importer/{entity_importer}/field-mapping/add",
 *     "edit-form" = "/admin/config/system/entity-importer/{entity_importer}/field-mapping/{entity_importer_field_mapping}/edit",
 *     "delete-form" = "/admin/config/system/entity-importer/{entity_importer}/field-mapping/{entity_importer_field_mapping}/delete"
 *   }
 * )
 */
class EntityImporterFieldMapping extends EntityImporterConfigEntityBase implements EntityImporterFieldMappingInterface {

  /**
   * The ID.
   */
  public string $id;

  /**
   * Field label.
   */
  public string $label;

  /**
   * The field mapping machine source name.
   */
  public ?string $name = NULL;

  /**
   * The field mapping source.
   */
  public ?string $source = NULL;

  /**
   * Contains plugins and configurations.
   */
  public array $processing = [];

  /**
   * The field mapping destination.
   */
  public ?string $destination = NULL;

  /**
   * The field mapping importer type.
   */
  public string $importer_type;

  /**
   * The field mapping importer bundle.
   */
  public ?string $importer_bundle = NULL;

  /**
   * {@inheritdoc}
   */
  public function id(): ?string {
    return isset($this->importer_type, $this->importer_bundle, $this->name)
      ? "{$this->importer_type}.{$this->importer_bundle}.{$this->name}"
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function name(): ?string {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getSource(): ?string {
    return $this->source;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestination(): ?string {
    return $this->destination;
  }

  /**
   * {@inheritdoc}
   */
  public function getImporterType(): string {
    return $this->importer_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getImporterBundle(): ?string {
    return $this->importer_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    if ($importer_entity = $this->getImporterEntity()) {
      $importer_entity->onChange();
    }
    parent::postSave($storage, $update);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    if ($importer_entity = $this->getImporterEntity()) {
      $importer_entity->onChange();
    }
    parent::delete();
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): void {
    parent::calculateDependencies();

    if ($entity_importer = $this->getImporterEntity()) {
      $this->addDependency('config', $entity_importer->getConfigDependencyName());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessingPlugins(): array {
    return $this->processing['plugins'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessingConfiguration(): array {
    return $this->processing['configuration'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasProcessingPlugin(string $plugin_id): bool {
    return isset($this->getProcessingPlugins()[$plugin_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function setImporterType(string $type): EntityImporterFieldMappingInterface {
    $this->importer_type = $type;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getImporterEntity(): ?EntityImporterInterface {
    return $this->loadEntityImporterType($this->importer_type);
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $url_route_parameters = parent::urlRouteParameters($rel);
    $url_route_parameters['entity_importer'] = $this->getImporterType();
    return $url_route_parameters;
  }

  /**
   * Load entity importer type.
   *
   * @param string $entity_type_id
   *   The entity importer identifier.
   */
  protected function loadEntityImporterType(string $entity_type_id): ?EntityImporterInterface {
    /** @var \Drupal\entity_import\Entity\EntityImporterInterface|null $value */
    $value = $this->entityTypeManager()
      ->getStorage('entity_importer')
      ->load($entity_type_id);
    return $value;
  }

}
