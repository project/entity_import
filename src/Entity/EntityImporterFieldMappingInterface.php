<?php

namespace Drupal\entity_import\Entity;

/**
 * Define entity importer field mapping interface.
 */
interface EntityImporterFieldMappingInterface {

  /**
   * Get field source name.
   *
   * @return string|null
   *   The field mapping machine source name.
   */
  public function name(): ?string;

  /**
   * Get field source.
   *
   * @return string|null
   *   The field mapping source.
   */
  public function getSource(): ?string;

  /**
   * Get field destination.
   *
   * @return string|null
   *   The field mapping destination.
   */
  public function getDestination(): ?string;

  /**
   * Get importer type.
   *
   * @return string
   *   The field mapping importer type.
   */
  public function getImporterType(): string;

  /**
   * Get importer bundle.
   *
   * @return string|null
   *   The field mapping importer bundle.
   */
  public function getImporterBundle(): ?string;

  /**
   * Get importer processing plugins.
   *
   * @return array
   *   The field mapping processing plugins.
   */
  public function getProcessingPlugins(): array;

  /**
   * Get importer processing configuration.
   *
   * @return array
   *   The field mapping processing configuration.
   */
  public function getProcessingConfiguration(): array;

  /**
   * Has processing plugin.
   *
   * @param string $plugin_id
   *   The processing plugin identifier.
   *
   * @return bool
   *   Determine if the process exist.
   */
  public function hasProcessingPlugin(string $plugin_id): bool;

  /**
   * Set importer type.
   *
   * @param string $type
   *   The importer type.
   *
   * @return $this
   */
  public function setImporterType(string $type);

  /**
   * Get importer entity instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getImporterEntity(): ?EntityImporterInterface;

}
