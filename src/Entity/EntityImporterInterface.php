<?php

namespace Drupal\entity_import\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Define the entity importer interface.
 */
interface EntityImporterInterface extends EntityInterface {

  /**
   * On change to the importer.
   */
  public function onChange();

  /**
   * Get entity importer description.
   *
   * @return string
   *   The entity importer description.
   */
  public function getDescription(): string;

  /**
   * Get entity importer display page.
   *
   * @return bool
   *   The entity importer display page boolean.
   */
  public function exposeImporter(): bool;

  /**
   * Get entity importer source information.
   *
   * @return array
   *   An array of source information.
   */
  public function getMigrationSource(): array;

  /**
   * Get entity importer entity information.
   *
   * @return array
   *   An array of entity information.
   */
  public function getMigrationEntity(): array;

  /**
   * Create a entity importer url.
   *
   * @param string $route_name
   *   The URL route name.
   *
   * @return \Drupal\Core\Url
   *   The drupal URL instance.
   */
  public function createUrl(string $route_name): Url;

  /**
   * Create a entity importer link.
   *
   * @param string $text
   *   The link text.
   * @param string $route_name
   *   The link route name.
   *
   * @return \Drupal\Core\Link
   *   The drupal link instance.
   */
  public function createLink(string $text, string $route_name): Link;

  /**
   * Get the migration plugin id.
   *
   * @param string $bundle
   *   The bundle on which to retrieve.
   *
   * @return string
   *   The migration plugin ID.
   */
  public function getMigrationPluginId(string $bundle): string;

  /**
   * Get importer source plugin id.
   *
   * @return mixed
   *   The importer source plugin id.
   */
  public function getImporterSourcePluginId(): mixed;

  /**
   * Get importer source configuration.
   *
   * @return array
   *   The importer source plugin configuration.
   */
  public function getImporterSourceConfiguration(): array;

  /**
   * Get importer entity bundles.
   *
   * @return array
   *   An array of allowed entity bundles.
   */
  public function getImporterBundles(): array;

  /**
   * Get importer entity type.
   *
   * @return mixed
   *   The importer entity type.
   */
  public function getImporterEntityType(): mixed;

  /**
   * Get importer migration dependencies.
   *
   * @return array
   *   The importer migration dependencies.
   */
  public function getMigrationDependencies(): array;

  /**
   * Has importer page display changed.
   *
   * @return bool
   *   Determine if the page display has changed.
   */
  public function hasPageDisplayChanged(): bool;

  /**
   * Create migration instance.
   *
   * @param string $bundle
   *   The entity bundle name.
   * @param array $definition
   *   Additional definitions that should be merged.
   */
  public function createMigrationInstance(string $bundle, array $definition = []): MigrationInterface;

  /**
   * Get entity importer first bundle.
   */
  public function getFirstBundle(): string;

  /**
   * Entity importer has multiple bundles.
   */
  public function hasMultipleBundles(): bool;

  /**
   * Get entity importer field mapping.
   *
   * @return \Drupal\entity_import\Entity\EntityImporterFieldMappingInterface[]
   *   An array of entity importer field mapping.
   */
  public function getFieldMapping(): array;

  /**
   * Has field mappings.
   */
  public function hasFieldMappings(): bool;

  /**
   * Get field mapping options.
   */
  public function getFieldMappingOptions(): array;

  /**
   * Get missing unique identifiers.
   *
   * @param array $identifiers
   *   An array of identifiers to check against.
   *
   * @return array
   *   An array of missing unique identifiers.
   */
  public function getMissingUniqueIdentifiers(array $identifiers): array;

  /**
   * Has field mapping unique identifiers.
   *
   * @return bool
   *   Return TRUE if field mapping have unique identifiers defined.
   */
  public function hasFieldMappingUniqueIdentifiers(): bool;

  /**
   * Get field mapping unique identifiers.
   *
   * @return array
   *   An array of field mapping unique identifiers.
   */
  public function getFieldMappingUniqueIdentifiers(): array;

  /**
   * Get dependency migrations.
   *
   * @param string $bundle
   *   The entity bundle.
   * @param bool $order
   *   Order the migrations.
   * @param array $definition
   *   An array of migration definition values.
   */
  public function getDependencyMigrations(string $bundle, bool $order = TRUE, array $definition = []): array;

  /**
   * Load dependency migration by plugin id.
   *
   * @param string $plugin_id
   *   The migration plugin id.
   * @param string $bundle
   *   The bundle associated with the required migration.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface|null
   *   Return the dependency migration instance; otherwise NULL.
   */
  public function loadDependencyMigration(string $plugin_id, string $bundle): ?MigrationInterface;

}
