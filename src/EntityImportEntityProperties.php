<?php

namespace Drupal\entity_import;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use function assert;

/**
 * Define the entity properties service.
 */
class EntityImportEntityProperties implements EntityImportEntityPropertiesInterface {

  use StringTranslationTrait;

  /**
   * The typed configuration manager service.
   */
  protected TypedConfigManagerInterface $typedConfigManager;

  /**
   * The entity field manager service.
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity properties constructor.
   */
  public function __construct(
    TypedConfigManagerInterface $typed_config_manager,
    EntityFieldManagerInterface $entity_field_manager,
  ) {
    $this->typedConfigManager = $typed_config_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityPropertiesOptions(EntityTypeInterface $entity_type, string $bundle): array {
    $options = [];
    $entity_class = $entity_type->getOriginalClass();

    if (is_subclass_of($entity_class, ConfigEntityInterface::class)) {
      assert($entity_type instanceof ConfigEntityTypeInterface);
      $options = $this->getEntityTypedDataMappingOptions(
        $entity_type
      );
    }

    if (is_subclass_of($entity_class, FieldableEntityInterface::class)) {
      assert($entity_type instanceof ContentEntityTypeInterface);
      $options = $this->getEntityFieldPropertyOptions(
        $entity_type, $bundle
      );
    }

    return $options;
  }

  /**
   * Get the entity typed data mapping options.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityTypeInterface $entity_type
   *   The entity type instance.
   *
   * @return array
   *   An array of allowed typed data mapping options.
   */
  protected function getEntityTypedDataMappingOptions(
    ConfigEntityTypeInterface $entity_type,
  ): array {
    $options = [];
    $config_prefix = $entity_type->getConfigPrefix();

    if ($typed_data_key = $this->findTypedDataKeyByPrefix($config_prefix)) {
      $definition = $this->typedConfigManager->getDefinition($typed_data_key);

      if (isset($definition['mapping'])) {
        $excluded_types = [
          'sequence',
          '_core_config_info',
          'config_dependencies',
        ];
        foreach ($definition['mapping'] as $name => $info) {
          if (in_array($info['type'], $excluded_types, TRUE)) {
            continue;
          }
          $options[$name] = $info['label'] ?? $name;
        }
      }
    }

    return $options;
  }

  /**
   * Get the entity field property options.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type
   *   The entity type identifier.
   * @param string $bundle
   *   The entity bundle type.
   *
   * @return array
   *   An array of entity property options.
   */
  protected function getEntityFieldPropertyOptions(
    ContentEntityTypeInterface $entity_type,
    string $bundle,
  ): array {
    $options = [];

    $fields = $this->loadEntityFieldDefinitions($entity_type->id(), $bundle);

    foreach ($fields as $field_name => $field) {
      if ($field->isComputed()) {
        continue;
      }
      /** @var \Drupal\Core\Field\TypedData\FieldItemDataDefinitionInterface $item_definition */
      $item_definition = $field->getItemDefinition();

      foreach ($item_definition->getPropertyDefinitions() as $property_name => $data_definition) {
        if ($data_definition->isComputed() || $data_definition->isReadOnly()) {
          continue;
        }
        if ($item_definition->getMainPropertyName() === $property_name) {
          $options[(string) ($field_name)] = $this->t('@field_name', [
            '@field_name' => $field_name,
          ]);
        }
        else {
          $options["{$field_name}/{$property_name}"] = $this->t('@field_name/@property_name', [
            '@field_name' => $field_name,
            '@property_name' => $property_name,
          ]);
        }
      }
    }

    return $options;
  }

  /**
   * Find typed data key by entity configuration prefix.
   *
   * @param string $config_prefix
   *   The entity type configuration prefix.
   *
   * @return string|null
   *   Return the type data key based on the configuration prefix.
   */
  protected function findTypedDataKeyByPrefix(string $config_prefix): ?string {
    $typed_data_key = preg_grep(
      "/$config_prefix\.+/",
      array_keys($this->typedConfigManager->getDefinitions())
    );

    return $typed_data_key ? reset($typed_data_key) : NULL;
  }

  /**
   * Load the entity field definitions.
   *
   * @param string $entity_type
   *   The entity type identifier.
   * @param string $bundle
   *   The entity bundle type.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of entity field definitions.
   */
  protected function loadEntityFieldDefinitions(string $entity_type, string $bundle): array {
    return $this
      ->entityFieldManager
      ->getFieldDefinitions($entity_type, $bundle);
  }

}
