<?php

namespace Drupal\entity_import;

use Drupal\entity_import\Event\EntityImportEvents;
use Drupal\entity_import\Event\EntityImportMigrationStubEvent;
use Drupal\entity_import\Plugin\migrate\process\EntityImportProcessInterface;
use Drupal\migrate\Plugin\MigratePluginManagerInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Define entity import process manager.
 */
class EntityImportProcessManager implements EntityImportProcessManagerInterface {

  /**
   * Event dispatcher.
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Migration plugin manager.
   */
  protected MigrationPluginManagerInterface $migrationManager;

  /**
   * Migrate plugin manager.
   */
  protected MigratePluginManagerInterface $migratePluginManager;

  /**
   * Entity import process manager constructor.
   */
  public function __construct(
    EventDispatcherInterface $event_dispatcher,
    MigrationPluginManagerInterface $migration_manager,
    MigratePluginManagerInterface $migrate_plugin_manager,
  ) {
    $this->eventDispatcher = $event_dispatcher;
    $this->migrationManager = $migration_manager;
    $this->migratePluginManager = $migrate_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function createPluginInstance(
    $plugin_id,
    $configuration = [],
    ?MigrationInterface $migration = NULL,
  ) {
    $prepare_event = $this->eventDispatcher
      ->dispatch(
        new EntityImportMigrationStubEvent($plugin_id, []),
        EntityImportEvents::ENTITY_IMPORT_PREPARE_MIGRATION_STUB
      );
    $configuration += $prepare_event->getConfiguration();

    $migration = $migration ?? $this->migrationManager->createStubMigration($configuration);

    return $this
      ->migratePluginManager
      ->createInstance($plugin_id, $configuration, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationProcessInfo(): array {
    $info = [];

    foreach ($this->getPluginInstances() as $plugin_id => $instance) {
      $info['options'][$plugin_id] = $instance->getLabel();
      $info['instances'][$plugin_id] = $instance;
    }

    return $info;
  }

  /**
   * Get entity import process plugin instances.
   *
   * @return array
   *   An array plugin instances.
   */
  protected function getPluginInstances(): array {
    $definitions = [];

    foreach ($this->migratePluginManager->getDefinitions() as $plugin_id => $definition) {
      $interface = EntityImportProcessInterface::class;

      if (!is_subclass_of($definition['class'], $interface)) {
        continue;
      }
      $definitions[$plugin_id] = $this->createPluginInstance($plugin_id);
    }

    return $definitions;
  }

}
