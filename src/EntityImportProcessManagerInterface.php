<?php

namespace Drupal\entity_import;

use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Define entity import process manager interface.
 */
interface EntityImportProcessManagerInterface {

  /**
   * Get entity import migration process information.
   *
   * @return array
   *   An array of migration process information.
   */
  public function getMigrationProcessInfo(): array;

  /**
   * Create the migrate process instance.
   *
   * @param string $plugin_id
   *   The plugin instance identifier.
   * @param array $configuration
   *   An array of a plugin configurations.
   * @param \Drupal\migrate\Plugin\MigrationInterface|null $migration
   *   The migration instance.
   *
   * @return object
   *   The plugin instance.
   */
  public function createPluginInstance(
    string $plugin_id,
    array $configuration = [],
    ?MigrationInterface $migration = NULL,
  );

}
