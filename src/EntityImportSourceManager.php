<?php

namespace Drupal\entity_import;

use Drupal\entity_import\Plugin\migrate\source\EntityImportSourceInterface;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrateSourcePluginManager;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;

/**
 * Define entity import source.
 */
class EntityImportSourceManager implements EntityImportSourceManagerInterface {

  /**
   * The migration manager.
   */
  protected MigrationPluginManagerInterface $migrationManager;

  /**
   * The migrate source manager.
   */
  protected MigrateSourcePluginManager $migrateSourceManager;

  /**
   * Entity import source construct.
   */
  public function __construct(
    MigrateSourcePluginManager $migrate_source_manager,
    MigrationPluginManagerInterface $migration_manager,
  ) {
    $this->migrateSourceManager = $migrate_source_manager;
    $this->migrationManager = $migration_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceStubInstance(string $plugin_id, array $configuration = [], ?MigrationInterface $migration = NULL,): MigrateSourceInterface {
    $migration = $migration ?? $this->migrationManager->createStubMigration([]);

    return $this
      ->migrateSourceManager
      ->createInstance($plugin_id, $configuration, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitionAsOptions(): array {
    $options = [];

    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      $options[$plugin_id] = $definition->getLabel();
    }

    return $options;
  }

  /**
   * Get entity import definitions.
   *
   * @return array
   *   The entity import source definitions.
   */
  protected function getDefinitions(): array {
    $definitions = [];

    foreach ($this->migrateSourceManager->getDefinitions() as $plugin_id => $definition) {

      if (is_subclass_of($definition['class'], EntityImportSourceInterface::class)) {
        $definitions[$plugin_id] = $this->createSourceStubInstance($plugin_id);
      }
    }

    return $definitions;
  }

}
