<?php

declare(strict_types=1);

namespace Drupal\entity_import;

use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Provides the interface of entity import source manager.
 */
interface EntityImportSourceManagerInterface {

  /**
   * Get entity import definition options.
   *
   * @return array
   *   An array of import source definitions.
   */
  public function getDefinitionAsOptions(): array;

  /**
   * Create the migrate source stub instance.
   *
   * @param string $plugin_id
   *   The migrate source plugin identifier.
   * @param array $configuration
   *   The migration source configuration.
   * @param \Drupal\migrate\Plugin\MigrationInterface|null $migration
   *   The migration object.
   *
   * @return \Drupal\migrate\Plugin\MigrateSourceInterface
   *   Instance of migration source plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createSourceStubInstance(
    string $plugin_id,
    array $configuration = [],
    ?MigrationInterface $migration = NULL,
  ): MigrateSourceInterface;

}
