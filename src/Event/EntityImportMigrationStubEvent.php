<?php

declare(strict_types=1);

namespace Drupal\entity_import\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Define entity import migration stub event.
 */
class EntityImportMigrationStubEvent extends Event {

  /**
   * The plugin ID.
   */
  protected string $pluginId;

  /**
   * Configuration of the plugin.
   */
  protected array $configuration = [];

  /**
   * Entity import migration stub event constructor.
   *
   * @param string $plugin_id
   *   The migration plugin identifier.
   * @param array $configuration
   *   The migration existing configuration.
   */
  public function __construct(string $plugin_id, array $configuration = []) {
    $this->pluginId = $plugin_id;
    $this->configuration = $configuration;
  }

  /**
   * Get migration plugin identifier.
   *
   * @return string
   *   The migration plugin id.
   */
  public function getPluginId(): string {
    return $this->pluginId;
  }

  /**
   * Get migration plugin configuration.
   *
   * @return array
   *   An array of migration configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * Set a configuration value.
   *
   * @param string $name
   *   The configuration name directive.
   * @param mixed $value
   *   The configuration value.
   */
  public function setConfigurationValue(string $name, mixed $value): static {
    if (is_array($value)
      && isset($this->configuration[$name])
      && is_array($this->configuration[$name])) {
      $this->configuration[$name] = array_merge_recursive(
        $this->configuration[$name], $value
      );
    }
    else {
      $this->configuration[$name] = $value;
    }

    return $this;
  }

}
