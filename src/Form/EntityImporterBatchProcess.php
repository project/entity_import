<?php

namespace Drupal\entity_import\Form;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_import\Plugin\migrate\source\EntityImportSourceLimitIteratorInterface;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Define the importer batch process.
 */
class EntityImporterBatchProcess {

  /**
   * Batch migration import.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration object.
   * @param bool $update
   *   A boolean flag if the migration should update.
   * @param int $status
   *   The migration status that should be initialized.
   * @param array $context
   *   An array of batch contexts.
   */
  public static function import(
    MigrationInterface $migration,
    bool $update,
    int $status,
    array &$context,
  ): void {
    $action = 'import';
    /** @var \Drupal\entity_import\Plugin\migrate\source\EntityImportSourceInterface $source */
    $source = $migration->getSourcePlugin();

    if (!isset($context['results']['count'])) {
      $context['results']['count'] = 0;
    }

    if (!isset($context['results']['action'])) {
      $context['results']['action'] = $action;
    }

    if ($source instanceof EntityImportSourceLimitIteratorInterface) {
      $limit = $source->getLimitCount();
      $source->setBatch();

      if (empty($context['sandbox'])) {
        $context['sandbox'] = [];
        $context['sandbox']['batch'] = 0;
        $context['sandbox']['iterations'] = abs(ceil($source->count() / $limit));
      }
      $batch = &$context['sandbox']['batch'];

      $offset = $batch * $limit;

      $source->setLimitOffset($offset);
      $batch++;

      /*
       *  We need to know if we're operating in the first iteration of the batch
       *  processing to decide if we need to run
       *  $migration->getIdMap()->prepareUpdate()
       */
      $firstBatch = $batch === 1;

      if ($batch < $context['sandbox']['iterations']) {
        $source->skipCleanup();
      }
      $execute_status = static::executeMigration(
        $migration, $update, $status, $action, $firstBatch
      );
      $source->resetBaseIterator();

      $context['results']['count'] += $source->getLimitIteratorCount();
      $context['results']['migrations'][$migration->id()]['status'][$batch] = $execute_status;

      if ($context['sandbox']['batch'] != $context['sandbox']['iterations']) {
        $context['finished'] = $context['sandbox']['batch'] / $context['sandbox']['iterations'];
      }
    }
    else {
      $execute_status = static::executeMigration(
        $migration, $update, $status, $action
          );
      $context['results']['count'] += $source->count();
      $context['results']['migrations'][$migration->id()]['status'] = $execute_status;
    }

    $context['message'] = new TranslatableMarkup(
      'Running the migration @action process for "@label".',
      [
        '@label' => $migration->label(),
        '@action' => $action,
      ]
    );
  }

  /**
   * Batch migration rollback.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration object.
   * @param int $status
   *   The migration status that should be initialized.
   * @param array $context
   *   An array of batch contexts.
   */
  public static function rollback(
    MigrationInterface $migration,
    int $status,
    array &$context,
  ): void {
    $action = 'rollback';
    $context['message'] = new TranslatableMarkup(
      'Running the migration @action process for "@label".',
      [
        '@label' => $migration->label(),
        '@action' => $action,
      ]
    );
    if (!isset($context['results']['count'])) {
      $context['results']['count'] = 0;
    }
    $count = $migration->getIdMap()->processedCount();

    $execute_status = static::executeMigration(
      $migration, FALSE, $status, $action
    );

    $context['results']['action'] = $action;
    $context['results']['count'] += $count;
    $context['results']['migrations'][$migration->id()]['status'] = $execute_status;
  }

  /**
   * Batch finished callback.
   *
   * @param bool $success
   *   The batch success boolean.
   * @param array $results
   *   The batch results.
   */
  public static function finished(bool $success, array $results): void {
    $action = $results['action'];
    $message = $success === TRUE
      ? new TranslatableMarkup(
        '<p>The system successfully ran the @action process for @count records.</p>', [
          '@action' => $action,
          '@count' => $results['count'],
        ]
      )
      : new TranslatableMarkup(
        '<p>The system experienced a problem when executing @action.</p>', [
          '@action' => $action,
        ]
      );

    static::messenger()->addMessage(
      $message,
      $success
        ? MessengerInterface::TYPE_STATUS
        : MessengerInterface::TYPE_WARNING
    );
  }

  /**
   * Execute migration.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration to execute.
   * @param bool $update
   *   A boolean flag if the migration should update.
   * @param int $status
   *   The migration status that should be initialized.
   * @param string $action
   *   The migration executable action to perform.
   * @param bool $firstBatch
   *   A boolean flag if it's the first iteration of the batch.
   *
   * @return int|false
   *   Return FALSE when the action does not exist as method of
   *   \Drupal\migrate\MigrateExecutableInterface else return value
   *   of the called method.
   */
  protected static function executeMigration(
    MigrationInterface $migration,
    bool $update,
    int $status,
    string $action,
    bool $firstBatch = TRUE,
  ): bool|int {
    $migration->setStatus($status);

    // When processing data in batches we need to call prepareUpdate() on idMap
    // only once or we'll rewrite all row statuses in each consecutive request.
    if ($update && $firstBatch === TRUE) {
      $migration->getIdMap()->prepareUpdate();
    }
    $executable = new MigrateExecutable($migration);

    if (!method_exists($executable, $action)) {
      return FALSE;
    }

    return call_user_func_array([$executable, $action], []);
  }

  /**
   * Get messenger instance.
   */
  protected static function messenger(): MessengerInterface {
    return \Drupal::service('messenger');
  }

}
