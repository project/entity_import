<?php

namespace Drupal\entity_import\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\entity_import\EntityImportSourceManagerInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function is_string;

/**
 * Define entity importer form.
 */
class EntityImporterForm extends EntityForm {

  /**
   * Route builder.
   */
  protected RouteBuilderInterface $routeBuilder;

  /**
   * Entity type bundle info.
   */
  protected EntityTypeBundleInfoInterface $entityBundleInfo;

  /**
   * Entity import source manager.
   */
  protected EntityImportSourceManagerInterface $entityImportSources;

  /**
   * Migration plugin manager.
   */
  protected MigrationPluginManagerInterface $migrationPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->routeBuilder = $container->get('router.builder');
    $instance->entityBundleInfo = $container->get('entity_type.bundle.info');
    $instance->entityImportSources = $container->get('entity_import.source.manager');
    $instance->migrationPluginManager = $container->get('plugin.manager.migration');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\entity_import\Entity\EntityImporterInterface $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('Label for the entity importer.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [
          $entity, 'entityExist',
        ],
      ],
      '#disabled' => !$entity->isNew(),
    ];
    $form['expose_importer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Expose importer'),
      '#description' => $this->t('The entity importer will be available from the listing page.'),
      '#default_value' => $entity->exposeImporter(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $entity->getDescription(),
    ];
    $form['migration_config'] = [
      '#title' => $this->t('Migration Configuration'),
      '#type' => 'vertical_tabs',
    ];
    $form['migration_source'] = [
      '#type' => 'details',
      '#group' => 'migration_config',
      '#title' => $this->t('Source'),
      '#tree' => TRUE,
    ];
    $form['migration_source']['source'] = [
      '#prefix' => '<div id="entity-importer-migration-source">',
      '#suffix' => '</div>',
    ];
    $migration_source = $entity->getMigrationSource();

    $source_plugin_id = $form_state->getValue(
      ['migration_source', 'source', 'plugin_id'],
      $migration_source['plugin_id'] ?? NULL
    );

    $form['migration_source']['source']['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Source Plugin'),
      '#required' => TRUE,
      '#options' => $this->getSourceOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'entity-importer-migration-source',
        'callback' => [$this, 'ajaxReloadCallback'],
      ],
      '#default_value' => $source_plugin_id,
    ];

    if (is_string($source_plugin_id) && $source_plugin_id !== '') {
      $configuration = $form_state->getValue(
        ['migration_source', 'source', 'configuration'],
        $migration_source['configuration'] ?? []
      );
      $source_plugin = $this
        ->entityImportSources
        ->createSourceStubInstance($source_plugin_id, $configuration);

      if ($source_plugin instanceof PluginFormInterface) {
        $subform = ['#parents' => ['migration_source', 'source', 'configuration']];
        $form['migration_source']['source']['configuration'] = $source_plugin
          ->buildConfigurationForm(
            $subform,
            $form_state
          );
      }
    }

    $form['migration_entity'] = [
      '#type' => 'details',
      '#group' => 'migration_config',
      '#title' => $this->t('Entity'),
      '#tree' => TRUE,
    ];
    $form['migration_entity']['entity'] = [
      '#prefix' => '<div id="entity-importer-migration-entity">',
      '#suffix' => '</div>',
    ];
    $migration_entity = $entity->getMigrationEntity();

    $options = $this->getEntityOptions();
    $entity_type = $form_state->getValue(
      ['migration_entity', 'entity', 'type'],
      $migration_entity['type'] ?? NULL
    );

    $form['migration_entity']['entity']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $options['entities'],
      '#empty_option' => $this->t('- Select -'),
      '#required' => TRUE,
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'entity-importer-migration-entity',
        'callback' => [$this, 'ajaxReloadCallback'],
      ],
      '#default_value' => $entity_type,
    ];

    if (!empty($entity_type)) {
      $entity_bundle = $form_state->getValue(
        ['migration_entity', 'entity', 'bundles'],
        $migration_entity['bundles'] ?? []
      );
      $form['migration_entity']['entity']['bundles'] = [
        '#type' => 'select',
        '#title' => $this->t('Bundles'),
        '#description' => $this->t('Select all bundles that can be imported.'),
        '#options' => $options['bundles'][$entity_type],
        '#multiple' => TRUE,
        '#required' => TRUE,
        '#default_value' => $entity_bundle,
      ];
    }
    $form['migration_dependencies'] = [
      '#type' => 'details',
      '#group' => 'migration_config',
      '#title' => $this->t('Dependencies'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];
    $dependencies = $entity->getMigrationDependencies();

    $form['migration_dependencies']['optional']['migration'] = [
      '#type' => 'select',
      '#title' => $this->t('Dependencies'),
      '#options' => $this->getMigrationOptions(),
      '#multiple' => TRUE,
      '#default_value' => $dependencies['optional']['migration'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $return = parent::save($form, $form_state);

    /** @var \Drupal\entity_import\Entity\EntityImporterInterface $entity */
    $entity = $this->entity;

    $this->rebuildRouteCache();

    if (!$entity->hasFieldMappings()) {
      $form_state->setRedirect('entity.entity_importer_field_mapping.collection', [
        'entity_importer' => $this->entity->id(),
      ]);
      return $return;
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $return;
  }

  /**
   * Ajax reload callback.
   */
  public function ajaxReloadCallback(array $form, FormStateInterface $form_state): mixed {
    $trigger = $form_state->getTriggeringElement();

    return NestedArray::getValue(
      $form, array_slice($trigger['#array_parents'], 0, -1)
    );
  }

  /**
   * Get entity form value.
   *
   * @param string|array $property
   *   The entity property name or an array of nested properties.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param mixed $default
   *   (optional) The default value.
   *
   * @return mixed
   *   The entity form value.
   */
  protected function getEntityFormValue(string|array $property, FormStateInterface $form_state, mixed $default = NULL): mixed {
    $property = !is_array($property)
      ? [$property]
      : $property;

    $array = (array) $this->entity;
    $value = $form_state->hasValue($property)
      ? $form_state->getValue($property)
      : NestedArray::getValue($array, $property);

    if (!empty($value)) {
      return $value;
    }

    return $default;
  }

  /**
   * Rebuild route cache.
   */
  protected function rebuildRouteCache(): void {
    /** @var \Drupal\entity_import\Entity\EntityImporterInterface $entity */
    $entity = $this->entity;

    if ($entity->hasPageDisplayChanged()) {
      $this->routeBuilder->rebuild();
    }
  }

  /**
   * Get source options.
   */
  protected function getSourceOptions(): array {
    return $this->entityImportSources->getDefinitionAsOptions();
  }

  /**
   * Get entity options.
   *
   * @return array
   *   An array of entity options.
   */
  protected function getEntityOptions(): array {
    $entity_info = [];

    foreach ($this->entityTypeManager->getDefinitions() as $plugin_id => $definition) {
      $class = $definition->getOriginalClass();
      $interface = FieldableEntityInterface::class;

      if (!is_subclass_of($class, $interface)) {
        continue;
      }
      $entity_info['entities'][$plugin_id] = $definition->getLabel();

      if ($bundles = $this->getEntityBundleOptions($definition->id())) {
        $entity_info['bundles'][$plugin_id] = $bundles;
      }
    }

    return $entity_info;
  }

  /**
   * Get entity bundle options.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of entity bundle options.
   */
  protected function getEntityBundleOptions(string $entity_type_id): array {
    $options = [];

    foreach ($this->entityBundleInfo->getBundleInfo($entity_type_id) as $name => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$name] = $definition['label'];
    }

    return $options;
  }

  /**
   * Get migration options.
   *
   * @return string[]
   *   An array of migration options.
   */
  protected function getMigrationOptions(): array {
    $options = [];

    foreach ($this->migrationPluginManager->getDefinitions() as $plugin_id => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$plugin_id] = $definition['label'];
    }

    return $options;
  }

}
