<?php

namespace Drupal\entity_import\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_import\Entity\EntityImporterInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function is_string;

/**
 * Define the entity importer action form.
 */
class EntityImporterPageImportActionForm extends EntityImporterBundleFormBase {

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Migration plugin manager.
   */
  protected MigrationPluginManagerInterface $migrationPluginManager;

  /**
   * Entity importer page import form.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MigrationPluginManagerInterface $migration_plugin_manager,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->migrationPluginManager = $migration_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.migration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_import_importer_action';
  }

  /**
   * Set the form title.
   *
   * @param \Drupal\entity_import\Entity\EntityImporterInterface $entity_importer
   *   The entity importer instance.
   */
  public function setTitle(EntityImporterInterface $entity_importer): TranslatableMarkup {
    return $this->t('@label: Action', [
      '@label' => $entity_importer->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    EntityImporterInterface $entity_importer = NULL,
  ): array {
    $form = parent::buildForm($form, $form_state, $entity_importer);
    if (!$entity_importer) {
      return $form;
    }

    $bundle = $this->getBundle();

    if (empty($bundle)) {
      return $form;
    }
    $form['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#options' => $this->getMigrationActionOptions(),
      '#required' => TRUE,
      '#empty_option' => $this->t('- None -'),
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => "entity-importer-bundle-form",
        'callback' => [$this, 'ajaxReplaceCallback'],
      ],
    ];
    $migrations = $entity_importer->getDependencyMigrations($bundle);

    $form['migrations'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Importers'),
      '#header' => [
        'title' => $this->t('Title'),
        'status' => $this->t('Status'),
      ],
      '#options' => $this->buildMigrationTableOptions($migrations),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['execute'] = [
      '#type' => 'submit',
      '#value' => $this->t('Execute'),
    ];

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $action = $form_state->getValue('action');
    $migrations = array_filter($form_state->getValue('migrations'));

    if (!is_string($action)) {
      return;
    }
    $actions = $this->getMigrationActionOptions();
    $operations = $this->buildMigrationActionOperations($action, $migrations);

    $this->setImporterBatchProcess($operations, $form_state, [
      'title' => $this->t('Executing @action', [
        '@action' => $actions[$action],
      ]),
    ]);
  }

  /**
   * Build migration action operations.
   *
   * @param string $action
   *   The migration action.
   * @param array $migrations
   *   An array of migration plugin ids.
   *
   * @return array
   *   An array of batch operations.
   */
  protected function buildMigrationActionOperations(string $action, array $migrations): array {
    $classes = [
      'rollback' => '\Drupal\entity_import\Form\EntityImporterBatchProcess::rollback',
    ];
    $operations = [];

    foreach ($migrations as $plugin_id) {
      $migration = $this->migrationPluginManager->createInstance($plugin_id);
      $operations[] = [
        $classes[$action],
        [$migration, MigrationInterface::STATUS_IDLE],
      ];
    }

    return $operations;
  }

  /**
   * Build migrations table options.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface[] $migrations
   *   An array of migrations.
   *
   * @return array
   *   An array of migration table options.
   */
  protected function buildMigrationTableOptions(array $migrations): array {
    $options = [];

    foreach ($migrations as $plugin_id => $migration) {
      $options[$plugin_id] = [
        'title' => $migration->label(),
        'status' => $migration->getStatusLabel(),
      ];
    }

    return $options;
  }

  /**
   * Get migration action options.
   */
  protected function getMigrationActionOptions(): array {
    return [
      'rollback' => $this->t('Rollback'),
    ];
  }

}
