<?php

declare(strict_types=1);

namespace Drupal\entity_import\Plugin\migrate\process;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides an interface for the entity import process plugins.
 */
interface EntityImportProcessInterface extends PluginFormInterface {

  /**
   * Process label.
   */
  public function getLabel(): string|TranslatableMarkup;

  /**
   * Default configurations.
   */
  public function defaultConfigurations(): array;

}
