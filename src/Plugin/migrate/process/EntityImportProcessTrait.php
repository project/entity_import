<?php

namespace Drupal\entity_import\Plugin\migrate\process;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;

/**
 * Define entity import process.
 */
trait EntityImportProcessTrait {

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return (string) $this->getPluginDefinition()['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfigurations(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * Ajax process callback.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   An array of the form state.
   */
  public function ajaxProcessCallback(array $form, FormStateInterface $form_state): array {
    $trigger = $form_state->getTriggeringElement();

    return NestedArray::getValue(
      $form, array_splice($trigger['#array_parents'], 0, -1)
    );
  }

  /**
   * Get the form state value.
   *
   * @param string|array $property
   *   The form property key, either a string or an array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   * @param null $default_value
   *   The default value.
   *
   * @return mixed
   *   The form state value.
   */
  protected function getFormStateValue(
    string|array $property,
    FormStateInterface $form_state,
    $default_value = NULL,
  ): mixed {
    if (!is_array($property)) {
      $property = [$property];
    }
    $config = $this->getConfiguration();
    $value = $form_state->hasValue($property)
      ? $form_state->getValue($property)
      : NestedArray::getValue($config, $property);

    if (empty($value)) {
      return $default_value;
    }

    return $value;
  }

  /**
   * Get process configuration.
   *
   * @return array
   *   An array of process configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration + $this->defaultConfigurations();
  }

}
