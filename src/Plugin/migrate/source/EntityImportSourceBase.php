<?php

namespace Drupal\entity_import\Plugin\migrate\source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_import\Entity\EntityImporterInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function is_array;
use function is_string;

/**
 * Define the entity import source base.
 */
abstract class EntityImportSourceBase extends SourcePluginBase implements EntityImportSourceInterface, ContainerFactoryPluginInterface {

  /**
   * Check source required.
   */
  protected bool $required = FALSE;

  /**
   * The entity_import.settings.
   */
  protected ImmutableConfig $settings;

  /**
   * Set to TRUE to skip the cleanup.
   */
  protected bool $skipCleanup = FALSE;

  /**
   * The entity importer plugin.
   */
  protected ?EntityImporterInterface $entityImporter = NULL;

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->entityTypeManager = $entity_type_manager;
    $this->settings = $config_factory->get('entity_import.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration = NULL,
  ): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return (string) $this->getPluginDefinition()['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function skipCleanup(): static {
    $this->skipCleanup = TRUE;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function runCleanup() {
  }

  /**
   * {@inheritdoc}
   */
  public function isRequired(): bool {
    return $this->required;
  }

  /**
   * Set source as required.
   */
  public function setRequired(): static {
    $this->required = TRUE;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    $fields = [];

    $importer = $this->getEntityImporter();

    foreach ($importer->getFieldMapping() as $field_mapping) {
      $fields[$field_mapping->name()] = $field_mapping->label();
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {
    $unique_ids = [];
    $importer = $this->getEntityImporter();

    foreach ($importer->getFieldMappingUniqueIdentifiers() as $info) {
      if (!isset($info['reference_type'], $info['identifier_type'])) {
        continue;
      }
      $reference_type = $info['reference_type'];
      $identifier_type = $info['identifier_type'];

      $identifier_name = $reference_type === 'field_type'
        ? $info['identifier_name']
        : $identifier_type;

      $unique_ids[$identifier_name]['type'] = $identifier_type;

      if (is_string($info['identifier_settings'])) {
        $settings = json_decode($info['identifier_settings'], TRUE);

        if (is_array($settings) && $settings !== []) {
          $unique_ids[$identifier_name] + $settings;
        }
      }
    }

    return $unique_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function buildImportForm(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateImportForm(array $form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function submitImportForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration = array_merge_recursive(
      $this->configuration, $form_state->cleanValues()->getValues()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * Get source default configuration.
   *
   * @return array
   *   An array of default configurations.
   */
  protected function defaultConfiguration(): array {
    return [];
  }

  /**
   * Get migrate source configuration.
   */
  protected function getConfiguration(): array {
    return $this->configuration + $this->defaultConfiguration();
  }

  /**
   * Get the entity importer instance.
   *
   * @return \Drupal\entity_import\Entity\EntityImporterInterface
   *   The entity importer instance.
   *
   * @throws \Drupal\migrate\MigrateException
   */
  protected function getEntityImporter(): EntityImporterInterface {
    if (!$this->entityImporter) {
      /** @var \Drupal\entity_import\Entity\EntityImporterInterface|null $value */
      $value = $this->entityTypeManager
        ->getStorage('entity_importer')
        ->load($this->getImporterId());
      $this->entityImporter = $value;
    }

    return $this->entityImporter;
  }

  /**
   * Get importer identifier.
   *
   * @return string
   *   The importer identifier.
   *
   * @throws \Drupal\migrate\MigrateException
   */
  protected function getImporterId(): string {
    $configuration = $this->getConfiguration();

    if (!isset($configuration['importer_id'])) {
      throw new MigrateException(
        'The importer_id directive in the migrate source is required.'
      );
    }

    return $configuration['importer_id'];
  }

}
