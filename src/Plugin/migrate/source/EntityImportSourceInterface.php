<?php

namespace Drupal\entity_import\Plugin\migrate\source;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\migrate\Plugin\MigrateSourceInterface;

/**
 * Define entity import source interface.
 */
interface EntityImportSourceInterface extends MigrateSourceInterface, PluginFormInterface {

  /**
   * Source is valid.
   */
  public function isValid(): bool;

  /**
   * Get source label.
   */
  public function getLabel(): string;

  /**
   * Skip source cleanup process.
   */
  public function skipCleanup(): static;

  /**
   * Run source clean up tasks.
   */
  public function runCleanup();

  /**
   * Check source required.
   */
  public function isRequired(): bool;

  /**
   * Set source as required.
   */
  public function setRequired(): static;

  /**
   * Build import form.
   *
   * @param array $form
   *   An array of the form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form elements.
   */
  public function buildImportForm(array $form, FormStateInterface $form_state): array;

  /**
   * Validate import form.
   *
   * @param array $form
   *   An array of the form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function validateImportForm(array $form, FormStateInterface $form_state);

  /**
   * Submit import form.
   *
   * @param array $form
   *   An array of the form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function submitImportForm(array &$form, FormStateInterface $form_state);

}
