<?php

namespace Drupal\entity_import\Plugin\migrate\source;

/**
 * Define the entity import source limit iterator base class.
 */
abstract class EntityImportSourceLimitIteratorBase extends EntityImportSourceBase implements EntityImportSourceLimitIteratorInterface {

  /**
   * The limit offset.
   */
  protected int $limitOffset = 0;

  /**
   * Is source operating in batch.
   */
  protected bool $isBatch = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getLimitCount(): int {
    return 10;
  }

  /**
   * {@inheritdoc}
   */
  public function getLimitOffset(): int {
    return $this->limitOffset;
  }

  /**
   * {@inheritdoc}
   */
  public function setLimitOffset(int $offset): static {
    $this->limitOffset = $offset;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setBatch(bool $is_batch = TRUE): static {
    $this->isBatch = $is_batch;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function resetBaseIterator(): static {
    $this->iterator = NULL;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function initializeIterator(): \LimitIterator {
    return new \LimitIterator(
      $this->limitedIterator(), $this->getLimitOffset(), $this->getLimitCount()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLimitIteratorCount(): int {
    $iterator = $this->getIterator();

    if ($iterator instanceof \Countable) {
      return $iterator->count();
    }

    return iterator_count($this->getIterator());
  }

}
