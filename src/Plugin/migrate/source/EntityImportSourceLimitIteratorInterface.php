<?php

declare(strict_types=1);

namespace Drupal\entity_import\Plugin\migrate\source;

/**
 * Define entity import source limit iterator interface.
 */
interface EntityImportSourceLimitIteratorInterface {

  /**
   * An iterator that needs to be limited.
   */
  public function limitedIterator(): \Iterator;

  /**
   * Get the iterator limit count.
   */
  public function getLimitCount(): int;

  /**
   * Get the iterator limit offset.
   */
  public function getLimitOffset(): int;

  /**
   * Set the iterator limit offset.
   *
   * @param int $offset
   *   The limit offset for the iterator.
   */
  public function setLimitOffset(int $offset): static;

  /**
   * Set the isBatch flag.
   *
   * @param bool $is_batch
   *   Is the operation being processed as a batch.
   */
  public function setBatch(bool $is_batch = TRUE): static;

  /**
   * Reset the base iterator.
   *
   * @return static
   *   The entity import csv source plugin.
   */
  public function resetBaseIterator(): static;

  /**
   * Get the iterator max count.
   *
   * @return int
   *   The limit iterator count.
   */
  public function getLimitIteratorCount(): int;

}
