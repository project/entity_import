<?php

declare(strict_types=1);

namespace Drupal\entity_import\Routing\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Symfony\Component\Routing\Route;
use function array_key_exists;
use function is_array;
use function is_string;

/**
 * Provides a param converter implementation of a migration plugin.
 */
final class MigrationConverter implements ParamConverterInterface {

  /**
   * Migration plugin manager.
   */
  protected MigrationPluginManagerInterface $migrationPluginManager;

  /**
   * The migration param converter.
   */
  public function __construct(MigrationPluginManagerInterface $migration_plugin_manager,) {
    $this->migrationPluginManager = $migration_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    if (is_string($value) && $value !== '') {
      return $this->migrationPluginManager->createInstance($value);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    return (is_array($definition) && array_key_exists('type', $definition) && $definition['type'] === 'migration');
  }

}
